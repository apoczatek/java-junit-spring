package pl.poczatek.junit.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"pl.poczatek.junit.spring"})
public class AppConfig {
}
