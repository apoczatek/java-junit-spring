package pl.poczatek.junit.spring;

public interface DataModelService {

    boolean isValid(String input);

}
